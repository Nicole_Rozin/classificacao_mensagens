#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Autora: Nicole A. Rozin Hervis (niarozin@gmail.com)

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from data_prep import DataPrep


# Define as palavras irrelevantes nas três linguagens presente nos dados
STOPS = (stopwords.words('portuguese') + stopwords.words('english') +
         stopwords.words('spanish'))


class RedundanceReduction:
    """
    Reduz as sentenças redudantes em duas etapas:
    1 - A partir da limpeza dos dados, identifica os duplicados e remove;
    2 - Dada uma frequência mínima (k) remove as setenças duplicadas,
        considerando apenas as palavras que aparecem mais de k vezes no
        conjunto de dados.

    -> Entrada: Nome do arquivo de mensagens;
    -> Saída: Indices de mensagens menos redundantes.
    """
    def __init__(self, filename):
        # Leitura e preparação inicial dos dados
        dp = DataPrep(filename)

        # Remove das mensagens as palavras identificadas como links
        dp.rm_feature("HTTPS")
        dp.rm_feature("HTTP")
        dp.rm_feature("WWW")

        # Limpa o texto
        dp.text_cleaner()
        self.data = dp.data

        # Remove as mensagens duplicadas após a limpeza
        self.index = self.data.drop_duplicates(keep="first").index

    def word_counts(self):
        # Conta as palavras desconsiderando as irrelevantes (NLTK)
        vectorizer = CountVectorizer(stop_words=STOPS)

        # Tranforma as sentenças em uma matriz (n sentenças) X (n palavras)
        X = vectorizer.fit_transform(self.data.SMS.iloc[self.index])
        X = X.toarray()

        # Lê as palavras resultantes
        words = vectorizer.get_feature_names()
        # Obtém a frequência das palavras
        self.freq = X.sum(axis=0)

        # Transforma em um dataframe
        self.X = pd.DataFrame(X, columns=words)

    def by_word_frequency(self, k=2):
        # Identifica as palavras com frequência minima
        idx = np.where(self.freq >= k)[0]
        # Seleciona as palavras com frequencia minima
        X = self.X.iloc[:, idx]

        # Identifica as senteças unicas nessas condições
        index = X.drop_duplicates(keep="first").index
        return self.index[index]
