#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Autora: Nicole A. Rozin Hervis (niarozin@gmail.com)


import pandas as pd


# Define conjunto de expressoes regulares para identificar info. nas mensagens
REGEX = {"HTTPS": r"https\:\S/(\w|/|\-|\.|\?|=|\d)*",
         "HTTP": r"http\:\S/(\w|/|\-|\.|\?|=|\d)*",
         "WWW": r"www\.\S(\w|/|\-|\.|\?|=|\d)*",
         "CPF": r"cpf(\:\s|\s)((\d{3}|[x]{3})(\.|\-)){3}\d{2}",
         "DATA": r"\s\d{1,4}(/|\-)\d{1,2}(/|\-)\d{1,4}",
         "HORARIO": r"\d{1,2}\:\d{2}(\s|[h]|:\d{2}\s)",
         "NoIP": r"(^|\s)(\d{1,3}\.){3}(\d{1,3}){1}(\s|)$",
         "VALOR_REAIS": r"r\$"}


class DataPrep:
    """
    Realiza a limpeza dos dados, desde exclusão a adição de informação.

    - Entrada: Aqruivo de mensagens]

    Obs: As funções add e rm_feature devem ser executadas antes de text_cleaner
    """
    def __init__(self, filename):
        # Leitura dos dados
        self.data = pd.read_csv(filename)

        # Configura para letras minúsculas
        self.data["SMS"] = self.data.SMS.str.lower()

        # Define cada mensagem em uma única linha
        self.data["SMS"] = self.data.SMS.replace(r"\n+", " ", regex=True)

    def add_feature(self, feature):
        # Padroniza uma característica para a informação no padrão indicado
        self.data["SMS"] = self.data.SMS.replace(REGEX[feature], feature,
                                                 regex=True)

    def rm_feature(self, feature):
        # Remove das mensagens palavras com o padrão indicado
        self.data["SMS"] = self.data.SMS.replace(REGEX[feature], " ",
                                                 regex=True)

    def text_cleaner(self):
        # Exclui números e palavras com letras e números
        clean = self.data.SMS.replace(r"[0-9]+[A-Z]*", " ", regex=True)

        # Exclui caracteres especiais
        clean = clean.replace(r'\W+', ' ', regex=True)
        clean = clean.replace(r"\_", " ", regex=True)

        # Exclui palavras de uma única letra
        clean = clean.replace(r"\s\w\s+", " ", regex=True)

        # Exclui nome próprio antecedido por pronome de tratamento sr/sra
        clean = clean.replace(r"(^|\s)sr(\s|[a]\s|\.\s)\w+\s", " ", regex=True)

        # Exclui multiplos x
        clean = clean.replace(r"[x]{2,}", " ", regex=True)

        # Exclui multiplos espaços
        clean = clean.replace(r"\s+", " ", regex=True)

        # Atualiza as mensagens
        self.data["SMS"] = clean
