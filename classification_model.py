#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Autor: Nicole A. Rozin Hervis (niarozin@gmail.com)

import argparse
import pickle
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import RidgeClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from reduncance_reduction import RedundanceReduction, STOPS


# Define os parametros dos modelos para a busca em grade
PRTS = {"general": {"tfidf__lowercase": [False],
                    "tfidf__stop_words": [STOPS],
                    "tfidf__binary": [False, True],
                    "tfidf__norm": ["l1", "l2"],
                    "tfidf__use_idf": [True, False],
                    "tfidf__max_df": [0.9, 0.95, 1.],
                    "tfidf__min_df": [0.01, 0.05, 0.1, 1],
                    "tfidf__max_features": [100, 200, 500, None]},
        "random_forest": {"clf__random_state": [42],
                          "clf__n_estimators": [10, 20, 50, 100, 200],
                          "clf__criterion": ['gini', 'entropy'],
                          "clf__max_depth": [5, 10, 15, 20, 25, 40, None],
                          "clf__max_features": ['sqrt', 'auto', 'log2']},
        "knn": {"clf__n_neighbors": [3, 5, 7, 11, 13, 17],
                "clf__weights": ['uniform', 'distance'],
                "clf__algorithm": ['auto', 'ball_tree', 'kd_tree'],
                "clf__metric": ["euclidean", "manhattan"]},
        "naive_bayes": {"clf__alpha": (1e-3, 1e-2, 0.1, 0.5, 1.),
                        "clf__fit_prior": (True, False)},
        "ridge": {"clf__alpha": (0.1, 0.2, 0.5, 0.8, 1.0),
                  "clf__fit_intercept": (True, False),
                  "clf__normalize": (False, True),
                  "clf__solver": ('auto', 'svd', 'sparse_cg', 'sag', 'saga')}}


# Define os métodos de classificação
METHODS = {"random_forest": RandomForestClassifier,
           "knn": KNeighborsClassifier,
           "naive_bayes": MultinomialNB,
           "ridge": RidgeClassifier}


class Model:
    def __init__(self, df):
        self.X_train = df.SMS.values
        print(len(self.X_train))
        self.y_train = np.where(df.LABEL == "ok", 0, 1)

    def search_model(self, method, cv):
        # Define o pipeline
        text_clf = Pipeline([("tfidf", TfidfVectorizer()),
                             ("clf", METHODS[method]())])

        # Junta os parametros de classificação e extração de caracteristica
        prts = {**PRTS["general"], **PRTS[method]}

        # Define a busca em grade
        gs_clf = GridSearchCV(text_clf, prts, cv=cv, n_jobs=-1, refit="f1",
                              scoring=["f1", "recall", "precision"])

        # Executa a busca em grade
        gs_clf.fit(self.X_train, self.y_train)

        # Salva os resultados e os melhores parametros
        result = {"best": gs_clf.best_params_, "results": gs_clf.cv_results_}
        print(result["best"])

        f = open("data/prts_%s" % (method), "wb")
        pickle.dump(result, f)
        f.close()

    def train_model(self, method):
        # Lê o resultado da busca em grade
        f = open("data/prts_%s" % (method), "rb")
        result = pickle.load(f)
        f.close()

        prts_clf = {key.split("__")[1]: result["best"][key] for key in PRTS[method].keys()}
        prts = {key.split("__")[1]: result["best"][key] for key in PRTS["general"].keys()}
        
        # Treina o modelo
        text_clf = Pipeline([("tfidf", TfidfVectorizer(**prts)),
                             ("clf", METHODS[method](**prts_clf))])

        text_clf.fit(self.X_train, self.y_train)
        return text_clf

    def test_predict(self, method, X_test):
        clf = self.train_model(method)

        # Testa o modelo
        y_pred = clf.predict(X_test)
        return y_pred


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Execução da busca em grade
    parser.add_argument("-m", "--method", type=str,
                        help='Classification method',
                        choices=["random_forest", "knn", "ridge",
                                 "naive_bayes"],
                        required=True)

    args = parser.parse_args()

    rr = RedundanceReduction("./data_train.csv")
    rr.word_counts()
    idx = rr.by_word_frequency(k=2)
    ml = Model(rr.data.iloc[idx])
    ml.search_model(args.method, 5)
